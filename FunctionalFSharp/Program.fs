module ListUtils

let rec length list =
  match list with
    | [] -> 0
    | h::t -> 1 + length t

let rec head list =
  match list with
    | h::_ -> h
    | [] -> failwith "list is empty"

let rec tail list =
  match list with
    | _::t -> t
    | [] -> failwith "list is empty"

let max2 x y =
  match (x, y) with
    | _ when  x > y -> x
    | _ -> y

let rec max list =
  match list with
    | [] -> failwith "list is empty"
    | [x] -> x
    | h::t -> max2 h (max t)

let rec zip list1 list2 =
  match list1, list2 with
    | h1::t1, h2::t2 -> (h1, h2) :: (zip t1 t2)
    | [], [] -> []
    | _ -> failwith "both list should be of same length"

let rec isSorted list =
  match list with
    | [] -> true
    | [_] -> true
    | h::t -> h <= (head t) && (isSorted t)

let rec index value list =
  match list with
    | [] -> -1
    | h::t when h = value -> 0
    | _::t ->
      let i = index value t
      match i with
        | -1 -> -1
        | x -> 1 + x


[<EntryPoint>]
let main (args : string[]) =
  0
